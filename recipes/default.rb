#
# Cookbook Name:: sphinx
# Recipe:: default
#
# Copyright 2014, Kazuki Seto
#
# All rights reserved - Do Not Redistribute
#

# INSTALL PACKAGE WITH YUM
%w{ python-setuptools python-devel zlib-devel freetype-devel libjpeg-turbo-devel python-imaging ipa-gothic-fonts }.each do |pckg|
	package "#{pckg}" do
		action :install
	end
end

# INSTALL PACKAGE WITH EASY_INSTALL
%w{ sphinx blockdiag sphinxcontrib-blockdiag }.each do |pckg|
	execute "easy_install #{pckg}" do
		command "sudo easy_install #{pckg}"
		action :run
	end
end

# reportlab
easy_install_package "reportlab" do
	version "2.5"
	action :install
end
